//
//  TransactionInput.swift
//  MoneyManager
//
//  Created by Thien Lang on 3/15/22.
//

import Foundation

enum TransactionInputType : String {
    case Income = "Income"
    case Expense = "Expense"
    case Transfer = "Transfer"
}

class TransactionInput {
    
    var type : TransactionInputType
    
    init(_ type: TransactionInputType) {
        self.type = type
    }
    
    init (tag : Int) {
        switch tag {
        case SelectedButtonTag.Income.rawValue:
            type = TransactionInputType.Income
        case SelectedButtonTag.Transfer.rawValue:
            type = TransactionInputType.Transfer
        default:
            type = TransactionInputType.Expense
        }
    }
    
    func changeType (_ type : TransactionInputType) {
        self.type = type
    }
    
    func getTitle() -> String {
        return type.rawValue
    }
}
