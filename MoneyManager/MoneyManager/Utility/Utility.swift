//
//  Utility.swift
//  MoneyManager
//
//  Created by Thien Lang on 3/14/22.
//

import Foundation

final class Utility {
    static let DATE_DDMM = "dd/M"
    static let DATE_DDMMYYYY = "dd/MM/yyyy (E)"
    
    static func formatSystemDate(format: String) -> String {
        let date = Date()
        let df = DateFormatter()
        df.dateFormat = format
        let dateString = df.string(from: date)
        return dateString
    }
}
