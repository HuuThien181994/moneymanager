//
//  InputTransViewController.swift
//  MoneyManager
//
//  Created by Thien Lang on 3/14/22.
//

import UIKit
import RealmSwift

protocol InputTransViewControllerDelegate {
    func completeFunction()
}

class InputTransViewController: UIViewController, UITextViewDelegate {

    var transInput : TransactionInput!
    
    @IBOutlet weak var segmentView: SegmentedCustomView!
    
    @IBOutlet weak var dateView: cusTextView!
    @IBOutlet weak var accountView: cusTextView!
    @IBOutlet weak var categoryView: cusTextView!
    @IBOutlet weak var amountView: cusTextView!
    @IBOutlet weak var feesView: cusTextView!
    @IBOutlet weak var noteView: cusTextView!
    @IBOutlet weak var descriptionTv: UITextView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    
    
    @IBOutlet weak var aboveView: UIView!
    private var colorHighlight : [UIColor] = [.blue, .red, .black]
    private var colorSelected : UIColor!
    
    private var listInput : [cusTextView]!
    private let bottomLineOfDescriptionTv = UIView()
    
    // Keyboard
    private var accountKeyboard: CustomKeyboardView!
    private var incomeCategoryKeyboard: CustomKeyboardView!
    private var expenseCategoryKeyboard: CustomKeyboardView!
    private var amountKeyboard: CustomKeyboardView!
    private var calendarView: CalendarView!
    private var transferToKeyboard: CustomKeyboardView!
    private var feeKeyboard: CustomKeyboardView!
    var delegate: InputTransViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        listInput = [dateView, accountView, categoryView, amountView, feesView, noteView]
        addRightButtonOnNavigationBar()
        segmentView.delegate = self
        
        self.initCustomKeyboard()
        
        saveButton.layer.cornerRadius = 10
        continueButton.layer.cornerRadius = 10
        continueButton.layer.borderWidth = 1
        continueButton.layer.borderColor = UIColor.gray.cgColor
        continueButton.clipsToBounds = true
        
        initDefaultValue()
        
        // Custom DescriptionTextView
        self.customInputDescription()
        
        self.eventTapOutEditTextField()
    }
    
    @IBAction func saveClicked(_ sender: UIButton) {
        let result = saveTransaction()
        if result {
            print("save successfully!")
        } else {
            print("check data is false!")
        }
        
        if let delegate = self.delegate {
            delegate.completeFunction()
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func continueClicked(_ sender: UIButton) {
        let result = saveTransaction()
        if result {
            print("save successfully!")
        } else {
            print("check data is false!")
        }
        clearDataForm(isFull: true)
    }
    
    func initDefaultValue() {
        clearDataForm(isFull: true)
        dateView.disabledCursor = true
        accountView.disabledCursor = true
        amountView.disabledCursor = true
        feesView.disabledCursor = true
        getButtonClicked(tag: segmentView.segmentSelected)
    }
    
    func initCustomKeyboard() {
        self.accountKeyboard = CustomKeyboardView(target: accountView.valueTextField, keyboardType: .AccountKeyboard)
        self.accountKeyboard.delegate = self
        accountView.setKeyboard(keyboard: self.accountKeyboard)
        
        self.transferToKeyboard = CustomKeyboardView(target: categoryView.valueTextField, keyboardType: .TransferToKeyboard)
        self.transferToKeyboard.delegate = self
        
        self.incomeCategoryKeyboard = CustomKeyboardView(target: categoryView.valueTextField, keyboardType: .IncomeCategoryKeyboard)
        self.incomeCategoryKeyboard.delegate = self
        
        self.expenseCategoryKeyboard = CustomKeyboardView(target: categoryView.valueTextField, keyboardType: .ExpenseCategoryKeyboard)
        self.expenseCategoryKeyboard.delegate = self

        self.amountKeyboard = CustomKeyboardView(target: amountView.valueTextField, keyboardType: .AmountKeyboard)
        self.amountKeyboard.delegate = self
        amountView.setKeyboard(keyboard: self.amountKeyboard)
        
        self.feeKeyboard = CustomKeyboardView(target: feesView.valueTextField, keyboardType: .FeeKeyboard)
        self.feeKeyboard.delegate = self
        feesView.setKeyboard(keyboard: self.feeKeyboard)
        
        self.calendarView = CalendarView(frame: CGRect(x: 0, y: 0, width: 400, height: 400))
        self.calendarView.didSelectDate = { [weak dateView] selectedDate in
            guard let mView = dateView else { return }
            mView.content = selectedDate?.date.fullDateFormat
        }
        self.calendarView.delegate = self
        dateView.setKeyboard(keyboard: self.calendarView)
    }
    
    func addRightButtonOnNavigationBar() {
        let barButtonItem = UIBarButtonItem(image: UIImage(systemName: "text.badge.star"), style: .done, target: self, action: #selector(btnBookmarkClicked))
        self.navigationItem.rightBarButtonItem = barButtonItem
    }
    
    @objc func btnBookmarkClicked(sender: UIBarButtonItem) {
        print("click bookmark!")
    }
    
    func setTitleForLabelOfInput(tag: Int) {
        dateView.caption = "Date"
        accountView.caption = "Account"
        categoryView.caption = "Category"
        amountView.caption = "Amount"
        feesView.caption = "Fees"
        noteView.caption = "Note"
        
        categoryView.content = nil
        hideFeesView()
        switch tag {
        case SelectedButtonTag.Transfer.rawValue:
            accountView.caption = "From"
            categoryView.caption = "To"
            categoryView.setKeyboard(keyboard: self.transferToKeyboard)
            displayFeesView()
            break
            
        case SelectedButtonTag.Income.rawValue:
            categoryView.setKeyboard(keyboard: self.incomeCategoryKeyboard)
            break
            
        case SelectedButtonTag.Expense.rawValue:
            categoryView.setKeyboard(keyboard: self.expenseCategoryKeyboard)
            break
            
        default:
            break
        }
        
//        if tag == SelectedButtonTag.Transfer.rawValue {
//            accountView.caption = "From"
//            categoryView.caption = "To"
//            categoryView.setKeyboard(keyboard: self.transferToKeyboard)
//            categoryView.content = nil
//
//            displayFeesView()
//        } else {
//            hideFeesView()
//            categoryView.setKeyboard(keyboard: self.categoryKeyboard)
//            categoryView.content = nil
//        }
    }
    
    private func hideFeesView() {
        feesView.isEnabled = false
    }
    
    private func displayFeesView() {
        feesView.isEnabled = true
    }
    
    func clearDataForm(isFull : Bool) {
        if (isFull) {
            dateView.content = Utility.formatSystemDate(format: Utility.DATE_DDMMYYYY)
            accountView.content = ""
            noteView.content = ""
            descriptionTv.text = ""
            amountView.content = "0"
            accountView.setFocus()
            segmentView.reDrawSelected()
        } else {
            setFocusActiveTextView()
        }

        categoryView.content = ""
        feesView.content = ""
    }
    
    // Custom Input: Description
    private func customInputDescription() {
        descriptionTv.delegate = self
        descriptionTv.isScrollEnabled = false
        
        self.resetPlaceHolderOfTextView()
        
        // Custom border
        bottomLineOfDescriptionTv.translatesAutoresizingMaskIntoConstraints = false
        bottomLineOfDescriptionTv.layer.shadowColor = UIColor.lightGray.cgColor
        bottomLineOfDescriptionTv.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        bottomLineOfDescriptionTv.layer.shadowRadius = 1.0
        bottomLineOfDescriptionTv.backgroundColor = .lightGray
        bottomLineOfDescriptionTv.layer.shadowOpacity = 1

        self.view.addSubview(bottomLineOfDescriptionTv)
        bottomLineOfDescriptionTv.topAnchor.constraint(equalTo: descriptionTv.bottomAnchor, constant: 1).isActive = true
        bottomLineOfDescriptionTv.leadingAnchor.constraint(equalTo: descriptionTv.leadingAnchor).isActive = true
        bottomLineOfDescriptionTv.trailingAnchor.constraint(equalTo: descriptionTv.trailingAnchor).isActive = true
        bottomLineOfDescriptionTv.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }
    
    private func resetPlaceHolderOfTextView() {
        if descriptionTv.text.isEmpty {
            descriptionTv.text = "Description"
            descriptionTv.textColor = UIColor.lightGray
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        bottomLineOfDescriptionTv.backgroundColor = colorSelected
        if descriptionTv.textColor == UIColor.lightGray {
            descriptionTv.text = nil
            descriptionTv.textColor = UIColor.black
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        bottomLineOfDescriptionTv.backgroundColor = .lightGray
        self.resetPlaceHolderOfTextView()
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView == descriptionTv {
            let size = CGSize(width: view.frame.width, height: .infinity)
            let estimatedSize = textView.sizeThatFits(size)
            
            textView.constraints.forEach { (constraint) in
                if constraint.firstAttribute == .height {
                    constraint.constant = estimatedSize.height
                }
            }
        }
    }
    
    private func eventTapOutEditTextField() {
        // endEditing
        let tap = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing))
        self.view.addGestureRecognizer(tap)
    }
    
    private func setFocusActiveTextView() {
        for item in listInput {
            if item.content!.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                item.setFocus()
                return
            }
        }
        descriptionTv.endEditing(true)
        descriptionTv.becomeFirstResponder()
    }
}

// Segment delegate
extension InputTransViewController : SegmentedCustomViewDelegate {
    func getButtonClicked(tag: Int) {
        colorSelected = colorHighlight[tag]
        transInput = TransactionInput(tag: tag)
        self.title = transInput.getTitle()
        
        for item in listInput {
            item.colorHighlight = colorSelected
        }
        
        setTitleForLabelOfInput(tag:tag)

        saveButton.backgroundColor = colorSelected
        clearDataForm(isFull: false)
    }
    
    
}

// CustomKeyboard
extension InputTransViewController: CustomKeyboardDelegate {
    // endEditing
    func btnCloseTap(view: CustomKeyboardView) {
        self.view.endEditing(true)
    }
}

extension InputTransViewController: CalendarDelegate {
    func btnTodayTap(date: Day?) {
        // print(date)
    }
    
    func btnCloseTap(view: CalendarView) {
        self.view.endEditing(true)
    }
    
}
