//
//  SaveTransaction.swift
//  MoneyManager
//
//  Created by Thien Lang on 3/21/22.
//

import UIKit
import RealmSwift


extension InputTransViewController {
    
    func checkData() -> Bool {
        if (dateView.content!.isEmpty) {
            dateView.setFocus()
            return false
        }
        
        if (accountView.content!.isEmpty) {
            accountView.setFocus()
            return false
        }
        
        if categoryView.content!.isEmpty {
            categoryView.setFocus()
            return false
        }
        
        return true
    }
    
    func saveTransaction() -> Bool {
        if !checkData() {
            return false
        }
        
        let data = Transactions()
        data.id = data.IncrementaID()
        data.date = dateView.content!
        data.account = accountView.content!
        data.category = categoryView.content!
        data.amount = Int(amountView.content!) ?? 0
        data.fees = Int(feesView.content!) ?? 0
        data.note = noteView.content!
        data.descriptions = descriptionTv.text
        data.typespend = segmentView.segmentSelected
        
        if !data.checkData() {
            return false
        }
        
        let realm = try! Realm()
        try! realm.write({ () -> Void in
            realm.add([data])
        })

        return true
    }
    
}
