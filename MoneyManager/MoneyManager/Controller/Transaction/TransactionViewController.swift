//
//  TransactionViewController.swift
//  MoneyManager
//
//  Created by Thien Lang on 3/14/22.
//

import UIKit
import RealmSwift

class TransactionViewController: UIViewController {

    var buttonAdd = UIButton(type: .custom)
    
    @IBOutlet weak var countLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // config navigation bar
        self.navigationItem.title = "Transaction"
        
        // create button add New transaction
        view.addSubview(buttonAdd)
        createButtonAddNew()
        setButtonAddConstraints()
        getCountDB()
        
    }

    func createButtonAddNew() {
        buttonAdd.layer.masksToBounds = true
        buttonAdd.clipsToBounds = true
        buttonAdd.setBackgroundImage(UIImage(systemName: "plus.circle.fill"), for: .normal)
        buttonAdd.addTarget(self, action: #selector(btnAddNewTransactionClicked), for: .touchUpInside)
        buttonAdd.tintColor = .red
        
        buttonAdd.layer.shadowColor = UIColor.darkGray.cgColor
        buttonAdd.layer.shadowOffset = CGSize(width: 1.5, height: 1.5)
        buttonAdd.layer.shadowRadius = 1.0
        buttonAdd.layer.shadowOpacity = 0.7
        
    }
    
    @objc func btnAddNewTransactionClicked() {
        let vc = InputTransViewController()
        vc.hidesBottomBarWhenPushed = true
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func setButtonAddConstraints() {
        buttonAdd.translatesAutoresizingMaskIntoConstraints = false
        buttonAdd.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -20).isActive = true
        buttonAdd.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant:  -20).isActive = true
        buttonAdd.heightAnchor.constraint(equalToConstant: 60).isActive = true
        buttonAdd.widthAnchor.constraint(equalToConstant: 60).isActive = true
    }
    
    func getCountDB() {
        let realm = try! Realm()
        let count = realm.objects(Transactions.self).count
        countLabel.text = String(count)
    }
}

extension TransactionViewController : InputTransViewControllerDelegate {
    func completeFunction() {
        getCountDB()
    }
}
