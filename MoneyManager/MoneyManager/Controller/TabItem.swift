//
//  TabItem.swift
//  MoneyManager
//
//  Created by Thien Lang on 3/14/22.
//

import UIKit

enum ItemType: String, CaseIterable {
    case transaction = "Transaction"
    case stats = "Stats"
    case accounts = "Accounts"
    case setting = "More"
    
    var displayTitle: String {
        return self.rawValue.capitalized(with: nil)
    }
}

class TabItem {
    
    var itemType : ItemType
    private var transactionViewController : TransactionViewController!
    private var statsViewController : StatsViewController!
    private var accountsViewController : AccountsViewController!
    private var settingViewController : SettingViewController!
    
    init(_ itemType : ItemType){
        self.itemType = itemType
        switch itemType {
        case .transaction:
            self.transactionViewController = TransactionViewController()
        case .stats:
            self.statsViewController = StatsViewController()
        case .accounts:
            self.accountsViewController = AccountsViewController()
        case .setting:
            self.settingViewController = SettingViewController()
        }
    }
   
    
    public var viewController: UIViewController {
        switch itemType {
        case .transaction:
            return transactionViewController!
        case .stats:
            return statsViewController!
        case .accounts:
            return accountsViewController!
        case .setting:
            return settingViewController!
        }
    }
    
    var icon: UIImage {
        switch itemType {
        case .transaction:
            return UIImage(systemName: "text.book.closed")!
        case .stats:
            return UIImage(systemName: "chart.bar")!
        case .accounts:
            return UIImage(systemName: "cylinder.split.1x2")!
        case .setting:
            return UIImage(systemName: "ellipsis")!
        }
    }
    
    var title : String {
        if (itemType == .transaction) {
            return Utility.formatSystemDate(format: Utility.DATE_DDMM)
        }
        return itemType.displayTitle
    }
}
