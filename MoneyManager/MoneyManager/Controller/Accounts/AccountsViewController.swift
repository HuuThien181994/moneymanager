//
//  AccountsViewController.swift
//  MoneyManager
//
//  Created by Thien Lang on 3/14/22.
//

import UIKit

class AccountsViewController: UIViewController {

    var calenderView: CalendarView!
        

        override func viewDidLoad() {
            super.viewDidLoad()
            
            calenderView = CalendarView()

            
            setupConstraints()
        }
        

        
        private func setupConstraints() {
            let mainStack = UIStackView(arrangedSubviews: [ calenderView])
            mainStack.axis = .vertical
            mainStack.translatesAutoresizingMaskIntoConstraints = false
            mainStack.spacing = 16
            view.addSubview(mainStack)
            
            NSLayoutConstraint.activate([
                mainStack.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                mainStack.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                mainStack.centerYAnchor.constraint(equalTo: view.centerYAnchor),
                
                calenderView.heightAnchor.constraint(equalToConstant: view.bounds.height / 2),
            ])
        }
    
}
