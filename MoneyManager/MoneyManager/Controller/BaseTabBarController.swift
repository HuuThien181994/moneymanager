//
//  BaseTabBarController.swift
//  MoneyManager
//
//  Created by Thien Lang on 3/14/22.
//

import UIKit

class BaseTabBarController: UITabBarController, UITabBarControllerDelegate{

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.loadTabbar()
    }
    

    func loadTabbar() {
        
        let tabbarItem : [TabItem] = [TabItem(.transaction), TabItem(.stats), TabItem(.accounts), TabItem(.setting)]
        var controllers = [UIViewController]()
        
        self.setupCustomTabMenu(tabbarItem)
        
        tabbarItem.forEach({
            let navigationViewController = UINavigationController(rootViewController: $0.viewController)
            controllers.append(navigationViewController)
        })
        self.viewControllers = controllers
        
        // Custom NavigationBar
        let navigationBarAppearance = UINavigationBar.appearance()
        navigationBarAppearance.tintColor = .black
    }
    
    /**
            @param: menuItems: an array of TabItem
     */
    func setupCustomTabMenu(_ menuItems: [TabItem]) {
        // set tabbar item
        for (idx, item) in 	menuItems.enumerated() {
            item.viewController.tabBarItem = UITabBarItem(title: item.title, image: item.icon, tag: idx)
        }
        
        self.tabBar.tintColor = .red
    
    }
    
    
}
