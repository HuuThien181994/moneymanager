//
//  Transactions.swift
//  MoneyManager
//
//  Created by Thien Lang on 3/21/22.
//

import Foundation
import RealmSwift

class Transactions : Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var date: String = ""
    @objc dynamic var account: String = ""
    @objc dynamic var category: String = ""
    @objc dynamic var amount : Int = 0
    @objc dynamic var note: String = ""
    @objc dynamic var fees: Int = 0
    @objc dynamic var descriptions: String = ""
    @objc dynamic var typespend : Int = 0
    
    //Incrementa ID
    func IncrementaID() -> Int{
        let realm = try! Realm()
        return (realm.objects(Transactions.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    
    override init() {
        super.init()
    }

    func checkData() -> Bool {
        if date.isEmpty {
            return false
        }
        
        if account.isEmpty {
            return false
        }
        
        if category.isEmpty {
            return false
        }
        
        return true
    }
}
