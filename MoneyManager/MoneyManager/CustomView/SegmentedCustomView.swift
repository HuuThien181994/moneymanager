//
//  SegmentedCustomView.swift
//  MoneyManager
//
//  Created by Thien Lang on 3/15/22.
//

import UIKit

protocol SegmentedCustomViewDelegate {
    func getButtonClicked(tag: Int)
}

enum SelectedButtonTag: Int {
    case Income
    case Expense
    case Transfer
}

@IBDesignable
class SegmentedCustomView: UIView {

    var contentView : UIView?
    var colorHighlight : [UIColor] = [.blue, .red, .black]
    var delegate: SegmentedCustomViewDelegate?
    private var listButtons : [UIButton]!
    private var segmentTagSelected : Int = 1
    
    var segmentSelected : Int {
        get {
            return segmentTagSelected
        }
    }
    
    @IBOutlet private weak var segment0: UIButton!
    @IBOutlet private weak var segment1: UIButton!
    @IBOutlet private weak var segment2: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
        
        listButtons = [segment0!, segment1!, segment2!]
        let listTag : [SelectedButtonTag] = [.Income, .Expense, .Transfer]
        
        for (idx, button) in listButtons.enumerated() {
            button.tintColor = colorHighlight[idx]
            button.layer.cornerRadius = 6
            button.layer.masksToBounds = true
            button.clipsToBounds = true
            button.tag = listTag[idx].rawValue
        }
        
        if segmentTagSelected >= 0 {
            drawSelected(listButtons[segmentTagSelected], tag:segmentTagSelected)
        }
        
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setup()
        contentView?.prepareForInterfaceBuilder()
    }
    
    func setup() {
        guard let view = loadViewFromNib() else { return }
        view.frame = bounds
        view.autoresizingMask =
            [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        contentView = view
    }
    
    func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "SegmentedCustomView", bundle: bundle)
        return nib.instantiate(
            withOwner: self,
            options: nil).first as? UIView
    }
    
    @IBAction func segmentClicked(_ sender: UIButton) {
        drawSegmentIfClicked(segmentTag: sender.tag)
        
        if let delegate = delegate {
            delegate.getButtonClicked(tag: sender.tag)
        }
    }
    
    func drawSegmentIfClicked(segmentTag : Int) {
        if segmentTag >= 0 && segmentTag <= listButtons.count {
            // delete style segment old
            if segmentTagSelected >= 0 {
                drawUnSelected(listButtons[segmentTagSelected])
            }
            
            // draw new button
            drawSelected(listButtons[segmentTag], tag:segmentTag)
            segmentTagSelected = segmentTag
        }
    }
    
    func drawSelected(_ sender : UIButton, tag : Int) {
        sender.layer.borderWidth = 1.0
        sender.layer.borderColor = colorHighlight[tag].cgColor
        sender.tintColor = colorHighlight[tag]
        sender.setTitleColor(colorHighlight[tag], for: .normal)
    }
    
    func drawUnSelected(_ sender : UIButton) {
        sender.layer.borderWidth = 0.0
        sender.tintColor = UIColor.darkGray
        sender.setTitleColor(UIColor.darkGray, for: .normal)
    }
    
    func reDrawSelected() {
        segmentClicked(segment1)
    }
}
