//
//  InputSelectWithCustomKeyboardField.swift
//  MoneyManager
//
//  Created by Hoang Minh on 17/03/2022.
//

import Foundation
import UIKit

class InputSelectWithCustomKeyboardField: UITextField {

    override init(frame: CGRect) {
        super.init(frame: frame)
        config()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        config()
    }
    
    func config() {
        self.backgroundColor = .green
        
        // Hide cursor in TextField when focus
        self.tintColor = .clear
    }

    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        // Avoid menu option show up
        return false
        
//        if action == #selector(paste(_:)) ||
//            action == #selector(cut(_:)) ||
//            action == #selector(copy(_:)) ||
//            action == #selector(select(_:)) ||
//            action == #selector(selectAll(_:)) ||
//            action == #selector(delete(_:)) ||
//            action == #selector(makeTextWritingDirectionLeftToRight(_:)) ||
//            action == #selector(makeTextWritingDirectionRightToLeft(_:)) ||
//            action == #selector(toggleBoldface(_:)) ||
//            action == #selector(toggleItalics(_:)) ||
//            action == #selector(toggleUnderline(_:)) {
//            return false
//        }
//
//        return super.canPerformAction(action, withSender: sender)
    }
    
    override var canBecomeFocused: Bool {
        return false
    }
    
}
