//
//  CustomKeyboardView.swift
//  MoneyManager
//
//  Created by Hoang Minh on 16/03/2022.
//

import UIKit

// MARK: - Delegate
protocol CustomKeyboardDelegate {
    func btnCloseTap(view: CustomKeyboardView)
}

enum KeyboardType: Int {
    case AccountKeyboard
    case IncomeCategoryKeyboard
    case ExpenseCategoryKeyboard
    case AmountKeyboard
    case CalendarKeyboard
    case TransferToKeyboard
    case FeeKeyboard
}

class CustomButton: UIButton {
    var content: String = ""
}

class CustomKeyboardView: NibLoadingView {
    // MARK: - Outlet
    @IBOutlet weak var labelTitleKeyboard: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var keyboardBody: UIView!
    
    // MARK: - Properties
    weak var target: (UIKeyInput & UITextInput)?
    var keyboardType: KeyboardType?
    var useDecimalSeparator: Bool?
    
    var keyList: [String]?
    var buttonList: [CustomButton]?
    
    var delegate: CustomKeyboardDelegate?
    
    init(target: UIKeyInput & UITextInput, keyboardType: KeyboardType, useDecimalSeparator: Bool = false) {
        super.init(frame: .zero)
        self.target = target
        self.keyboardType = keyboardType
        self.useDecimalSeparator = useDecimalSeparator
        
        configure()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }
    
    
    
    // MARK: - Actions
    @IBAction func btnClose_clicked(_ sender: UIButton) {
        if let delegate = delegate {
            delegate.btnCloseTap(view: self)
        }
    }
    
    @objc func replaceContentTextField(_ sender: CustomButton) {
        self.replaceWholeText("\(sender.content)")
    }
    
    @objc func didTapDigitButton(_ sender: CustomButton) {
        self.insertText("\(sender.content)")
    }
    
    @objc func didTapDecimalButton(_ sender: CustomButton) {
        insertText(Locale.current.decimalSeparator ?? ".")
    }
    
    @objc func didTapDeleteButton(_ sender: CustomButton) {
        target?.deleteBackward()
    }
}

// MARK: - Private function - Init
private extension CustomKeyboardView {
    func configure() {
        btnClose.setTitle("", for: .normal)
        
        // Fullsize
        autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Keylist:
        let accountList = ["Cash", "Bank accounts", "Credit Cards"]
        let incomeCategoryList = ["Allowance", "Salary", "Petty cash", "Bonous", "Other"]
        let expenseCategoryList = ["Food", "Social Life", "Self-development", "Transportation", "Culture", "Household", "Apparel", "Beauty", "Health", "Education", "Gift", "Other"]
        
        // Set title & get key's contents
        switch(self.keyboardType) {
            case .AccountKeyboard:
                labelTitleKeyboard.text  = "Account"
                self.keyList = accountList
                self.addKeys()
                break;
            
            case .TransferToKeyboard:
                labelTitleKeyboard.text  = "Account"
                self.keyList = accountList
                self.addKeys()
                break;
            
            case .IncomeCategoryKeyboard:
                labelTitleKeyboard.text  = "Category"
                self.keyList = incomeCategoryList
                self.addKeys()
                break;
            
            case .ExpenseCategoryKeyboard:
                labelTitleKeyboard.text  = "Category"
                self.keyList = expenseCategoryList
                self.addKeys()
                break;
            
            case .AmountKeyboard:
                labelTitleKeyboard.text  = "Amount"
                self.addKeysNumberPad();
                break;
            
            case .FeeKeyboard:
                labelTitleKeyboard.text  = "Fees"
                self.addKeysNumberPad();
                break;
            
            default:
                break;
        }
    }
    
    
    func addKeys() {
        let stackView = createStackView(axis: .vertical)
        stackView.frame = bounds
        stackView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        keyboardBody.addSubview(stackView)
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: keyboardBody.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: keyboardBody.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: keyboardBody.trailingAnchor)
        ])

        // Config layout keyboard
        let keyboardPerRow = 3    // TODO: config by width of device
        // let widthOfKey: CGFloat = CGFloat(UIScreen.main.bounds.width) / CGFloat(keyboardPerRow)
        let numberOfRows =  Int(round(Double(self.keyList!.count) / Double(keyboardPerRow)))
        
        // Add buton list
        var remainKeyboard = self.keyList!.count % keyboardPerRow
        if (remainKeyboard) > 0 {
            remainKeyboard = keyboardPerRow - remainKeyboard
            for _ in 0..<remainKeyboard {
                self.keyList?.append("")
            }
        }
        self.buttonList = [CustomButton]()
        for item in self.keyList! {
            let button = CustomButton(type: .system)
            
            if item != "" {
                button.content = item
                button.setTitle("\(item)", for: .normal)
                button.setTitleColor(.black, for: .normal)
                button.titleLabel?.font = UIFont.systemFont(ofSize: 15)
                button.layer.borderWidth = 0.3
                button.layer.borderColor = UIColor.gray.cgColor
                button.addTarget(self, action: #selector(replaceContentTextField(_:)), for: .touchUpInside)
            }
            
            self.buttonList!.append(button)
        }
        
        for row in 0 ..< numberOfRows {
            let subStackView = createStackView(axis: .horizontal)
            subStackView.backgroundColor = .white
            stackView.addArrangedSubview(subStackView)
            
            subStackView.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                subStackView.leadingAnchor.constraint(equalTo: stackView.leadingAnchor),
                subStackView.trailingAnchor.constraint(equalTo: stackView.trailingAnchor),
                subStackView.heightAnchor.constraint(equalToConstant: 35),
            ])
            
            for column in 0 ..< keyboardPerRow {
                let index = row * keyboardPerRow + column
                if index >= self.buttonList!.count {
                    break
                }
                guard let currentKey = self.buttonList?[index] else { break }
                subStackView.addArrangedSubview(currentKey)
            }
        }
    }
    
    func addKeysNumberPad() {
        // Init key NumberPad
        let numericButtons: [CustomButton] = (0...9).map {
            let button = CustomButton(type: .system)
            button.content = "\($0)"
            button.setTitle("\($0)", for: .normal)
            button.titleLabel?.font = .preferredFont(forTextStyle: .largeTitle)
            button.setTitleColor(.black, for: .normal)
            button.layer.borderWidth = 0.3
            button.layer.borderColor = UIColor.darkGray.cgColor
            // button.accessibilityTraits = [.keyboardKey]
            button.addTarget(self, action: #selector(didTapDigitButton(_:)), for: .touchUpInside)
            return button
        }

        let deleteButton: CustomButton = {
            let button = CustomButton(type: .system)
            button.setTitle("⌫", for: .normal)
            button.titleLabel?.font = .preferredFont(forTextStyle: .largeTitle)
            button.setTitleColor(.black, for: .normal)
            button.layer.borderWidth = 0.3
            button.layer.borderColor = UIColor.darkGray.cgColor
            button.accessibilityTraits = [.keyboardKey]
            button.accessibilityLabel = "Delete"
            button.addTarget(self, action: #selector(didTapDeleteButton(_:)), for: .touchUpInside)
            return button
        }()

        let decimalButton: UIButton = {
            let button = UIButton(type: .system)
            let decimalSeparator = Locale.current.decimalSeparator ?? "."
            button.setTitle(decimalSeparator, for: .normal)
            button.titleLabel?.font = .preferredFont(forTextStyle: .largeTitle)
            button.setTitleColor(.black, for: .normal)
            button.layer.borderWidth = 0.3
            button.layer.borderColor = UIColor.darkGray.cgColor
            button.accessibilityTraits = [.keyboardKey]
            button.accessibilityLabel = decimalSeparator
            button.addTarget(self, action: #selector(didTapDecimalButton(_:)), for: .touchUpInside)
            return button
        }()
        
        // Add button to View
        let stackView = createStackView(axis: .vertical)
        stackView.frame = bounds
        stackView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        keyboardBody.addSubview(stackView)

        // Numpad
        for row in 0 ..< 3 {
            let subStackView = createStackView(axis: .horizontal)
            stackView.addArrangedSubview(subStackView)

            for column in 0 ..< 3 {
                subStackView.addArrangedSubview(numericButtons[row * 3 + column + 1])
            }
        }

        let subStackView = createStackView(axis: .horizontal)
        stackView.addArrangedSubview(subStackView)

        if useDecimalSeparator! {
            subStackView.addArrangedSubview(decimalButton)
        } else {
            let blank = UIView()
            blank.layer.borderWidth = 0.5
            blank.layer.borderColor = UIColor.darkGray.cgColor
            subStackView.addArrangedSubview(blank)
        }

        subStackView.addArrangedSubview(numericButtons[0])
        subStackView.addArrangedSubview(deleteButton)
    }
    
    func createStackView(axis: NSLayoutConstraint.Axis) -> UIStackView {
        let stackView = UIStackView()
        stackView.axis = axis
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        return stackView
    }
    
    func insertText(_ string: String) {
        guard let range = target?.selectedRange else { return }

        if let textField = target as? UITextField, textField.delegate?.textField?(textField, shouldChangeCharactersIn: range, replacementString: string) == false {
            return
        }

        if let textView = target as? UITextView, textView.delegate?.textView?(textView, shouldChangeTextIn: range, replacementText: string) == false {
            return
        }

        target?.insertText(string)
    }
    
    func replaceWholeText(_ string: String) {
        guard let validTarget = target else { return }
        
        let textRange = validTarget.textRange(from: validTarget.beginningOfDocument, to: validTarget.endOfDocument)
        target?.replace(textRange!, withText: string)
    }
}

// MARK: - UITextInput extension
extension UITextInput {
    var selectedRange: NSRange? {
        guard let textRange = selectedTextRange else { return nil }

        let location = offset(from: beginningOfDocument, to: textRange.start)
        let length = offset(from: textRange.start, to: textRange.end)
        return NSRange(location: location, length: length)
    }
}
