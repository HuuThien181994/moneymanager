//
//  cusTextView.swift
//  MoneyManager
//
//  Created by Thien Lang on 3/15/22.
//

import UIKit

@IBDesignable
class cusTextView: UIView {

  
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueTextField: UITextField!
    private var contentView : UIView?
    var colorHighlight : UIColor = .red
    private let bottomLine = UIView()
    private var cusTintColor : UIColor!
//    private var keyboardType: KeyboardType?
//    var KeyboardType: KeyboardType? {
//        get { return self.keyboardType }
//        set {
//            self.keyboardType = newValue
//            if self.keyboardType != nil {
//                if keyboardType == .some(.CalendarKeyboard) {
//                    let calendarView = CalendarView(frame: CGRect(x: 0, y: 0, width: 400, height: 500))
//                    calendarView.didSelectDate = { [weak self] selectedDate in
//                                guard let self = self else { return }
//                                self.valueTextField.text = selectedDate?.date.fullDateFormat
//                            }
//                    self.valueTextField.inputView = calendarView
//
//                } else {
//                    self.valueTextField.inputView = CustomKeyboardView(target: self.valueTextField, keyboardType: keyboardType!)
//                }
//            }
//        }
//    }
    
    var caption: String! {
        get { return titleLabel?.text }
        set { titleLabel?.text = newValue }
    }

    var content: String! {
        get { return valueTextField?.text }
        set { valueTextField?.text = newValue }
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.cusTintColor = self.valueTextField.tintColor
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
        setupValueTextField()
    }
    
    func setupValueTextField() {
        valueTextField.borderStyle = .none
        valueTextField.delegate = self
        
        bottomLine.translatesAutoresizingMaskIntoConstraints = false
        
        bottomLine.layer.shadowColor = UIColor.darkGray.cgColor
        bottomLine.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        bottomLine.layer.shadowRadius = 1.0
        
        valueTextField.addSubview(bottomLine)
        
        bottomLine.topAnchor.constraint(equalTo: valueTextField.bottomAnchor, constant: 1).isActive = true
        bottomLine.leadingAnchor.constraint(equalTo: valueTextField.leadingAnchor).isActive = true
        bottomLine.trailingAnchor.constraint(equalTo: valueTextField.trailingAnchor).isActive = true
        bottomLine.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        bottomLine.backgroundColor = .lightGray
        bottomLine.layer.shadowOpacity = 0.0
        
        
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setup()
        contentView?.prepareForInterfaceBuilder()
    }
    
    func setup() {
        guard let view = loadViewFromNib() else { return }
        view.frame = bounds
        view.autoresizingMask =
            [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        contentView = view
    }
    
    func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "cusTextView", bundle: bundle)
        return nib.instantiate(
            withOwner: self,
            options: nil).first as? UIView
    }
    
    func setKeyboard(keyboard: UIView) {
        self.valueTextField.inputView = keyboard
    }
    
}

extension cusTextView : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        bottomLine.backgroundColor = colorHighlight
        bottomLine.layer.shadowOpacity = 0.7
        
        textField.selectedTextRange = nil; // hides caret

    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        bottomLine.backgroundColor = .lightGray
        bottomLine.layer.shadowOpacity = 0.0
    }
    
}

extension cusTextView {
    func setFocus() {
        self.valueTextField.endEditing(true)
        self.valueTextField.becomeFirstResponder()
    }
    
    var disabledCursor: Bool {
        get {
            if self.valueTextField.tintColor == .clear {
                return true
            }
            
            return false
        }
        set {
            if (newValue) {
                self.valueTextField.tintColor = .clear
            } else {
                self.valueTextField.tintColor = cusTintColor
            }
        }
    }
    var isEnabled: Bool {
        get {
            return self.valueTextField.isEnabled
        }
        set {
            self.valueTextField.isEnabled = newValue
        }
    }
    
}

extension cusTextView {

    enum Visibility {
        case visible
        case invisible
        case gone
    }

    var visibility: Visibility {
        get {
            let constraint = (self.constraints.filter{$0.firstAttribute == .height && $0.constant == 0}.first)
            if let constraint = constraint, constraint.isActive {
                return .gone
            } else {
                return self.isHidden ? .invisible : .visible
            }
        }
        set {
            if self.visibility != newValue {
                self.setVisibility(newValue)
            }
        }
    }

    private func setVisibility(_ visibility: Visibility) {
        let constraint = (self.constraints.filter{$0.firstAttribute == .height && $0.constant == 0}.first)

        switch visibility {
        case .visible:
            constraint?.isActive = false
            self.isHidden = false
            break
        case .invisible:
            constraint?.isActive = false
            self.isHidden = true
            break
        case .gone:
            if let constraint = constraint {
                constraint.isActive = true
            } else {
                let constraint = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 0)
                self.addConstraint(constraint)
                constraint.isActive = true
            }
        }
    }
}
